package pojo;

public class Singer {

	//Write a class Singer[name,gender,age,email_id,contact,rating]
			
		private String name, gender,email_id,contact;
	    private int  age,rating;
		public Singer(String name, String gender, String email_id, String contact, int age, int rating) {
			super();
			this.name = name;
			this.gender = gender;
			this.email_id = email_id;
			this.contact = contact;
			this.age = age;
			this.rating = rating;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getEmail_id() {
			return email_id;
		}
		public void setEmail_id(String email_id) {
			this.email_id = email_id;
		}
		public String getContact() {
			return contact;
		}
		public void setContact(String contact) {
			this.contact = contact;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public int getRating() {
			return rating;
		}
		public void setRating(int rating) {
			this.rating = rating;
		}
		@Override
		public String toString() {
			return "Singer [name=" + name + ", gender=" + gender + ", email_id=" + email_id + ", contact=" + contact
					+ ", age=" + age + ", rating=" + rating + "]";
		}
	    
	    
}
